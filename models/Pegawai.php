<?php namespace Yfktn\Pegawai\Models;

use Model;

/**
 * Model
 */
class Pegawai extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_pegawai_utama';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required'
    ];
        
    /**
     * Ini untuk foto pegawai
     * @var type 
     */
    public $attachOne = [
        'fotopegawai' => [ 'System\Models\File', 'public' => true ]
    ];
    
    public function getNamaLengkapAttributes() {
        return "{$this->attributes['gelar_depan']} {$this->attributes['nama']} {$this->attributes['gelar_belakang']}";
    }
    
    public function scopeListDiFrontEnd($query, $opsi = []) {
        extract(array_merge([
            'page' => 1,
            'jumlahItemPerHalaman' => 10,
            'filter' => [],
            'order' => []
        ], $opsi));
        
        if(sizeof($filter) > 0) {
            
        }
        
        if($order != []) {
            foreach ($order as $k=>$v ) {
                $query->orderBy($k, $v);
            }
        }        
        
        // kembalikan hasil paginate
//        return $query->paginate($jumlahItemPerHalaman, $page);        
    }
}
