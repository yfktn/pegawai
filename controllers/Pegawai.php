<?php namespace Yfktn\Pegawai\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;

class Pegawai extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'yfktn.pegawai.atur_pegawai' , 'yfktn.pegawai.atur_pegawai_lainnya'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.Pegawai', 'menu-pegawai');
    }
    
    public function formExtendQuery($query) {
        // lakukan deklarasi event, siapa tahu ada yang membutuhkan untuk
        // proses lebih lanjut, misalnya pada connector untuk melakukan perubahan
        // pada query
        Event::fire('yfktn.pegawai.formExtendQuery', [$query]);
    }
}
