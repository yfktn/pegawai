<?php namespace Yfktn\Pegawai;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Yfktn\Pegawai\Components\PegawaiList' => 'pegawaiList'
        ];
    }

    public function registerSettings()
    {
    }
}
