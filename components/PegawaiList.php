<?php namespace Yfktn\Pegawai\Components;
use Yfktn\Pegawai\Models\Pegawai as PegawaiModel;
/**
 * Description of PegawaiList
 *
 * @author The Happy Dude
 */
class PegawaiList extends \Cms\Classes\ComponentBase {
    //put your code here
    public function componentDetails() {
        return [
            'name' => 'Tampilkan Pegawai',
            'description' => "Tampilkan daftar pegawai"
        ];
    }

    public function defineProperties() {
        return [
            'paramHalaman' => [
                'title' => 'Parameter Halaman',
                'description' => 'Parameter menunjukkan halaman aktif yang di load',
                'type' => 'string',
                'default' => '{{ :page }}'
            ],
            'jumlahItemPerHalaman' => [
                'title' => 'Item Perhalaman',
                'description' => 'Jumlah item perhalaman ditampilkan',
                'type' => 'string',
                'default' => 10
            ],
            'urutRandom' => [
                'title' => 'Urut Random',
                'description' => 'Dapatkan daftar dengan urut random',
                'type' => 'checkbox',
                'default' => false
            ]
//            'halamanDetail' => [
//                'title' => 'Halaman Detail',
//                'description' => 'Alamat menuju detail tulisan',
//                'type' => 'dropdown',
//                'default' => 'tulisan/detail'
//            ]
        ];
    }
    
    protected function siapkanVariable() {
        $this->page['paramHalaman'] = $this->paramName('paramHalaman');
        $this->page['halamanAktif'] = $this->property('paramHalaman', 1);
        $this->page['urutRandom'] = $this->property('urutRandom');
        $this->page['jumlahItemPerHalaman'] = $this->property('jumlahItemPerHalaman');
    }
    
    protected function loadPegawai() {
        $posts = PegawaiModel::with(['fotopegawai'])
                ->listDiFrontEnd([
                    'page' => $this->page['halamanAktif'],
                    'jumlahItemPerHalaman' => $this->page['jumlahItemPerHalaman'],
                    'filter' => [],
                    'order' => []
                ]);
        if((bool)$this->page['urutRandom']) {
            // TODO: PLEASE optimalisasikan ini bila sudah datanya besar!
            $posts = $posts->inRandomOrder();
        }
        return $posts->paginate(
                $this->page['jumlahItemPerHalaman'], 
                $this->page['halamanAktif']);
    }
    
    public function onRun() {
        $this->siapkanVariable();
        $this->page['posts'] = $this->loadPegawai();
    }
    
}
