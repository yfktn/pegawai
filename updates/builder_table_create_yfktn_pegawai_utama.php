<?php namespace Yfktn\Pegawai\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnPegawaiUtama extends Migration
{
    public function up()
    {
        Schema::create('yfktn_pegawai_utama', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama', 100);
            $table->string('nomor_id', 30);
            $table->string('gelar_depan', 50)->nullable();
            $table->string('gelar_belakang', 50)->nullable();
            $table->text('bio')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('foto', 256)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('tempat_lahir', 100)->nullable();
            $table->tinyInteger('aktif')->default(1);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_pegawai_utama');
    }
}